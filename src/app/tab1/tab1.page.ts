import { Component } from '@angular/core';

import { Tab2Page } from '../tab2/tab2.page';

import { Tab3Page } from '../tab3/tab3.page';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  list = [
    {
      status:"1",
      nome: "Denys",
      convenio:"Bradesco",
      valor:"100.00"  
    },
    {
      status:"2",
      nome: "Mario",
      convenio:"Amil",
      valor:"200.00"  
    }
  ];


  constructor() {
    
  }
  

}
